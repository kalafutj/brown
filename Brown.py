#!/usr/bin/python3

import random
import math
import matplotlib.pyplot as plt

sqrt=math.sqrt
ln=math.log
sin=math.sin
u01=random.random
PI=math.pi

def normal_dist(sigma2=1):
	R=sqrt(-2*ln(u01()))
	Fi=sin(2*PI*u01())
	return sqrt(sigma2)*R*Fi
	
def brown_motion(n,ndiv):
	H=n/ndiv
	fx=[0.]
	
	for j in range(ndiv):
		fx.append(fx[-1]+normal_dist(H))
	
	return fx		
		
def brown_plotter(n,ndiv,nt):
	H=n/ndiv
	X=[]
	splna=0.
	for x in range(ndiv+1):
		X.append(H*x)
		
#	plt.axis([0.-n/20.,n+n/20.,-3*sqrt(n),3*sqrt(n)])
		
	for i in range(nt):
		fx=brown_motion(n,ndiv)
		if (fx[-1]>0):
			plt.plot(X,fx,'g-')
			plt.plot(X[-1],fx[-1],'bo')
			splna+=1
		else:
			plt.plot(X,fx,'r-')
			plt.plot(X[-1],fx[-1],'y^')
			
	plt.plot(0.,0.,'bo')
	
	print (splna)
	print (splna/nt)
	
	plt.show()

def brown_printer(n,ndiv,nt):
	H=n/ndiv
	X=[]
	for x in range(ndiv+1):
		X.append(H*x)
	
	print(str(X).replace('[','').replace(']',''))
	
	for i in range(nt):
		X=brown_motion(n,ndiv)
		print(str(X).replace('[','').replace(']',''))

def read_plot(name):
	X=[]
	fx=[]
	splna=0.
	pocet=0.
	subor=open(name)
	
	Xstr=subor.readline().split(', ')
	for x in Xstr:
		X.append(float(x))
	
	for line in subor:
		pocet+=1
		for x in line.split(', '):
			fx.append(float(x))
		
#		plt.axis([X[0]-0.02,X[-1]+0.02,-3*sqrt(X[-1]-X[0]),3*sqrt(X[-1]-X[0])])
					
		if (fx[-1]>0):
			plt.plot(X,fx,'g-')
			plt.plot(X[-1],fx[-1],'bo')
			splna+=1
		else:
			plt.plot(X,fx,'r-')
			plt.plot(X[-1],fx[-1],'y^')
		fx=[]
			
	plt.plot(0.,0.,'bo')
	
	print(splna)
	print(splna/pocet)
	
	plt.show()
